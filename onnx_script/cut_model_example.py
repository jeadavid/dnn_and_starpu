import sys
import onnx
from onnx import helper
from onnx import AttributeProto, TensorProto, GraphProto

input_path = "models/googlenet/googlenet_gpu/1/model.onnx"
output_path = ""

input_names = ["input"]
output_names = ["522","528","534","538"]
onnx.utils.extract_model(input_path, output_path+"0.onnx", input_names, output_names)

input_names = ["522","528","534","538"]
output_names = ["output"]
onnx.utils.extract_model(input_path, output_path+"1.onnx", input_names, output_names)