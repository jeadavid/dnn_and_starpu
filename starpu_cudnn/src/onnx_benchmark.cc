#include "onnx_benchmark.h"

#include <onnxruntime_cxx_api.h>
#include <cuda_runtime.h>

#include <fstream>
#include <iostream>
#include <string>
#include <numeric>
#include <chrono>

void onnx_benchmark_gpu(std::vector<long long> &benchmark_results, std::string &model_path, std::string &input_name, std::string &output_name, int device_id, int batch)
{
  std::vector<float> image(batch * 3 * 224 * 224, 150);
  std::vector<int64_t> input_dims = {batch, 3, 224, 224};
  std::vector<int64_t> output_dims = {batch, 1000};

  Ort::Env env(ORT_LOGGING_LEVEL_FATAL, "InferenceGPU");

  Ort::SessionOptions session_options;
  session_options.SetGraphOptimizationLevel(ORT_ENABLE_ALL);
  OrtStatus* status = OrtSessionOptionsAppendExecutionProvider_CUDA(session_options, device_id);
  if (status != nullptr) 
  {
    Ort::GetApi().ReleaseStatus(status);
    throw std::runtime_error("Erreur lors de l'ajout du fournisseur d'exécution CUDA");
  }
  Ort::Session session(env, model_path.c_str(), session_options);

  Ort::MemoryInfo info_cuda("Cuda", OrtAllocatorType::OrtArenaAllocator, device_id, OrtMemTypeDefault);  
  Ort::Allocator cuda_allocator(session, info_cuda);

  int num_iterations = 1000;
  Ort::IoBinding binding(session);

  benchmark_results.reserve(num_iterations);

  for (int i = 0; i < num_iterations+5; i++) 
  {
    auto input = cuda_allocator.GetAllocation(image.size() * sizeof(float));
    cudaMemcpyAsync(input.get(), image.data(), sizeof(float) * image.size(), cudaMemcpyHostToDevice);
    std::vector<float> output_data(std::accumulate(output_dims.begin(), output_dims.end(), 1, std::multiplies<int>()));
    auto output = cuda_allocator.GetAllocation(output_data.size() * sizeof(float));


    Ort::Value bound_X = Ort::Value::CreateTensor(info_cuda, reinterpret_cast<float*>(input.get()), image.size(), input_dims.data(), input_dims.size());
    Ort::Value bound_Y = Ort::Value::CreateTensor(info_cuda, reinterpret_cast<float*>(output.get()), output_data.size(), output_dims.data(), output_dims.size());  
    binding.BindInput(input_name.c_str(), bound_X);
    binding.BindOutput(output_name.c_str(), bound_Y);

    Ort::RunOptions run_options;
    run_options.AddConfigEntry("disable_synchronize_execution_providers", "1");
    auto start_gpu = std::chrono::high_resolution_clock::now();
    binding.SynchronizeInputs();
    session.Run(Ort::RunOptions(), binding);
    binding.SynchronizeOutputs();

    auto end_gpu = std::chrono::high_resolution_clock::now();
    auto duration_gpu = std::chrono::duration_cast<std::chrono::nanoseconds>(end_gpu - start_gpu);

    binding.ClearBoundInputs();
    binding.ClearBoundOutputs();

    // warmup the model before taking mesures
    if(i > 4)
      benchmark_results.push_back(duration_gpu.count()); // Store the result
  }
}

void onnx_benchmark_cpu(std::string &file_path, std::string &model_path, std::string &input_name, std::string &output_name, int batch)
{
  std::vector<float> image(batch * 3 * 224 * 224, 150);
  std::vector<int64_t> input_dims = {batch, 3, 224, 224};
  std::vector<int64_t> output_dims = {batch, 1000};

  Ort::Env env(ORT_LOGGING_LEVEL_WARNING, "InferenceCPU");
  Ort::SessionOptions session_options;
  session_options.SetIntraOpNumThreads(12);

  session_options.SetGraphOptimizationLevel(ORT_ENABLE_ALL);
  //session_options.EnableProfiling("cpu_profile_file");
  Ort::Session session(env, model_path.c_str(), session_options); 

  auto memory_info = Ort::MemoryInfo::CreateCpu(OrtArenaAllocator, OrtMemTypeDefault);

  int num_iterations = 1000;
  std::vector<Ort::Value> tensors;
  tensors.reserve(num_iterations); // Réserver de l'espace pour num_iterations éléments

  for (int i = 0; i < num_iterations; ++i) {
    Ort::Value tensor = Ort::Value::CreateTensor<float>(memory_info, image.data(), image.size(), input_dims.data(), input_dims.size());
    tensors.push_back(std::move(tensor));
  }

  const char* input_names[] = {input_name.c_str()};
  const char* output_names[] = {output_name.c_str()};

  // warmup 
  for (int i = 0; i < 10; i++)
  {
    session.Run(Ort::RunOptions{nullptr}, input_names, &tensors[i], 1, output_names, 1);
  }

  std::ofstream file(file_path, std::ios::app);
  auto start_cpu = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < num_iterations; i++)
  {
    session.Run(Ort::RunOptions{nullptr}, input_names, &tensors[i], 1, output_names, 1);
  }
  auto end_cpu = std::chrono::high_resolution_clock::now();
  auto duration_cpu = std::chrono::duration_cast<std::chrono::seconds>(end_cpu - start_cpu);
  std::cout<<"CPU inference duration : "<<duration_cpu.count()<< " sec" <<std::endl;
  std::cout<<"débit : "<<(num_iterations * batch) / (duration_cpu.count())<< " inf/sec" <<std::endl;
  file << batch << " " << duration_cpu.count() << "\n";
  file.close();
}