#ifndef STARPU_TENSOR_HPP
#define STARPU_TENSOR_HPP

#include <starpu.h>
#include <onnxruntime_cxx_api.h>

#include <vector>
#include <string>

#include "onnx.proto3.pb.h"

/**
 * Structure representing the essential information of a tensor.
 * 
 * This structure contains the basic details of a tensor, including its size,
 * the data type it contains, and information about its dimensions.
 */
struct TensorInfo
{
  int64_t size;                  // Total size (total number of elements).
  size_t data_size;              // Data size in bytes.
  size_t dim_size;               // Number of dimensions.
  ONNXTensorElementDataType type;// Tensor elements data type (e.g., float, int).
  char *name{nullptr};
  int64_t *dim_data{nullptr};    // Array containing the size of each dimension.
};

/**
 * Tensor represents a tensor for neural network computations.
 * 
 * This class encapsulates a tensor, providing functionalities for creating tensors,
 * managing their data, and interacting with computation devices. It supports
 * various data types and is compatible with ONNX runtime environments.
 */
class Tensor
{
 public:
  Tensor(const std::string& name, ONNXTensorElementDataType type, std::vector<int64_t> dims);
  Tensor(const std::string& name, const std::vector<int64_t>& dims, const std::vector<float>& vec);
  Tensor(const std::string& name, const std::vector<int64_t>& dims, const std::vector<int64_t>& vec);

  static ONNXTensorElementDataType GetTensorType(const std::string& name, const onnx::GraphProto& graph_proto);
  static const onnx::TypeProto* FindTensorTypeProto(const std::string& name, const onnx::GraphProto &graph_proto);

  size_t InfoSizeof() const;
  size_t TotalSizeInBytes() const;
  size_t GetDataSize(ONNXTensorElementDataType type) const;  
  
  TensorInfo ToInfo();
  void FreeData();
  void FreeOnDevice();
  void RegisterOnDevice(std::vector<std::string> &out_names);

 private:
  template<typename T>
  void* CreateData(const std::vector<T>& vec) const;

 public:
  std::string name;
  ONNXTensorElementDataType type;// Tensor elements data type (e.g., float, int).
  size_t data_size;              // Data size in bytes.
  int64_t size;                  // Total size (total number of elements).
  std::vector<int64_t> dims;
  void* data;
  double start_time = 0;             // Tensor start computing time.

  // Handle to manage the tensor data within StarPU. It acts as an opaque identifier
  // that allows StarPU to keep track of the tensor's data across the machine,
  // ensuring data consistency and managing data replication.
  starpu_data_handle_t handle;
};

#endif