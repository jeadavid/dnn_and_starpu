#include <string>
#include <vector>

struct InputOutput {
  std::string name;
  std::string data_type;
  std::vector<int> dims;
};

struct InstanceGroup {
  int count;
  std::string kind;
};

struct Config {
  std::string name;
  std::string platform;
  int max_batch_size;
  std::vector<InputOutput> inputs;
  std::vector<InputOutput> outputs;
  std::vector<InstanceGroup> instance_groups;
};

class ConfigParser {
 public:
  static Config parseConfig(const std::string& filename);

 private:
  static std::vector<int> parseDims(const std::string& line);
};