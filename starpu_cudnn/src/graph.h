#ifndef GRAPH_HPP
#define GRAPH_HPP

#include <list>

#include <mutex>
#include <future>
#include <atomic>
#include <condition_variable>

#include "tensor.h"

#define no_combined_workers -1
#define combined_worker_limit_id 200

// LayerInfo encapsulates the necessary information for managing a layer within a Graph.
//
// This structure is crucial for setting up and executing computations on layers in a
// parallel computing context managed by StarPU. It holds information about input and
// output tensors, sessions, and bindings required for ONNX Runtime (Ort) execution.
// LayerInfo is used in both CPU and GPU contexts to facilitate the correct binding
// and running of sessions based on the layer's data.
//
// Fields:
//   first_worker_id: ID of the first worker assigned to this layer. Used to manage
//                    thread binding in combined worker scenarios.
//   in_size: The number of input tensors for the layer.
//   out_size: The number of output tensors for the layer.
//   in: Array of TensorInfo objects representing information about input tensors.
//   out: Array of TensorInfo objects representing information about output tensors.
//   sessions: Array of pointers to Ort sessions, one for each worker. These sessions
//             are used to run the computations for the layer.
//   bindings: Array of pointers to Ort IoBinding objects, one for each worker. These
//             bindings are used to bind input and output tensors to the Ort sessions.
struct LayerInfo
{
  int first_worker_id;
  int in_size;
  int out_size;
  TensorInfo *in{nullptr};
  TensorInfo *out{nullptr};
  Ort::Session **sessions;
  Ort::IoBinding **bindings;
};

// GraphSubmit encapsulates a submission of a graph or sub-graph for execution.
//
// This class represents a single instance of a computational DNN graph or a part of it
// that is ready to be executed.
class GraphSubmit
{
 public:
  GraphSubmit(int id, int priority, std::unordered_map<std::string, Tensor> tensors_map) : id(id), priority(priority) ,tensors_map(tensors_map){};

  int id;
  int priority;
  std::unordered_map<std::string, Tensor> tensors_map;
};

// SubGraph represents a subsection of a larger DNN computational graph.
//
// This structure is used to manage a distinct segment of a computation, typically
// part of a larger model, in a parallel computing environment. Each sub-graph
// contains its own set of input and output tensors and is identified by a unique key
// and name. The SubGraph structure is integral to handling different parts of a model
// independently.
struct SubGraph
{
  int key;
  std::string name;
  std::vector<Tensor*> inputs;
  std::vector<Tensor*> outputs;
};

// SessionArg holds the necessary arguments for initializing sessions in a Graph.
//
// This structure is used to pass context and resources required for setting up
// ONNX Runtime (Ort) sessions for different sub-graphs of a computational graph.
// It includes paths, environment settings, and mappings for sessions and bindings,
// facilitating the management of resources across different worker types in a
// StarPU-driven parallel computing environment.
//
// Fields:
//   path: Base path for the ONNX model files.
//   ort_env: Pointer to the ONNX Runtime environment shared across sessions.
//   sub_graphs: Pointer to a vector of sub-graphs, each representing a portion of the
//               computational graph.
//   idx_sessions: Pointer to a map linking sub-graph keys to their corresponding session indices.
//   idx_bindings: Pointer to a map linking sub-graph keys to their corresponding binding indices.
//   sessions: Pointer to a vector of vectors, each containing Ort sessions for a specific worker.
//   bindings: Pointer to a vector of vectors, each containing Ort IoBindings for a specific worker.
//
// The structure is primarily used in the Graph::InitializeSessions method to create and
// manage sessions for different sub-graphs based on the type of worker (CPU or GPU).
// It ensures that each sub-graph is associated with the correct Ort session and binding,
// allowing efficient execution of the graph on heterogeneous computing resources.
struct SessionArg
{
  std::string path;
  Ort::Env *ort_env;
  std::vector<SubGraph> *sub_graphs;
  std::unordered_map<int, int> *idx_sessions;
  std::unordered_map<int, int> *idx_bindings;
  std::vector<std::vector<Ort::Session>> *sessions;
  std::vector<std::vector<Ort::IoBinding>> *bindings;
};

// CallbackInfo holds context data for a callback associated with a StarPU task.
// 
// This structure is used to pass additional information to the callback function
// that is executed after a StarPU task completes. It includes pointers to various
// data structures that might be needed within the callback, such as tensors and
// binding information for input and output. It is designed to facilitate
// asynchronous task management and data handling in a StarPU-driven parallel
// computing environment.
struct CallbackInfo
{
  GraphSubmit *graph_submit;
  double *latence;
  int in_size;
  int out_size;
  Tensor **in_tensors;
  Tensor **out_tensors;
  Ort::IoBinding **bindings;
  char *out_name;
  char *in_name;  
  int *is_running;  //Pointer to an integer, indicating if the task is currently running.
  int *worker_id;
};

// Graph manages the lifecycle and execution of a DNN graph for neural network inference.
//
// This class is responsible for setting up, managing, and executing a DNN computational graph
// using the ONNX Runtime (Ort) environment. It supports operations on both CPU and GPU, 
// handling the allocation and execution of tasks in a parallel computing environment, 
// by using StarPU for task scheduling. The Graph is capable of executing a graph
// that is partitioned into multiple sub-graphs, allowing for distribution across different
// processors. This feature is particularly beneficial for optimizing
// the computation on heterogeneous computing architectures. The class provides
// functionalities for initializing sessions, running inferences, managing tensor data, and
// handling callback information for asynchronous execution. 
//
// TODO : For the future
// [It orchestrates the overall
// computational flow, ensuring that different segments of the graph are processed on the
// appropriate hardware, thereby maximizing efficiency and performance.]
class Graph
{
 public:
  Graph(Ort::Env *ort_env, const std::string& path, int graph_count);
  ~Graph() {
    std::cout << "---------- graph deleted ---------\n" << std::endl;
  }  

  void FreeGraph();
  void InitializeRun(int latence_size, int nb_combined_workers_);
  void Run(int batch, int task_id, int priority, int worker_id, int inference_num, int &is_running, bool is_sync, std::vector<std::string> &in_names, std::vector<std::string> &out_names, std::vector<Tensor>& in_tensors, std::vector<Tensor>& out_tensors);

  std::vector<int> GetWorkerIds();
  std::vector<double> GetLatences();

  template<typename T>
  void AllocateConcurrency(int nb_concurrency, std::string &in_name, std::vector<int64_t> &tensors_dims, std::vector<std::vector<Tensor>> &tensors) {
    int64_t total_size = 1;
    for (auto dim : tensors_dims) {
      if (dim <= 0 || total_size > (INT64_MAX / dim)) {
        throw std::overflow_error("Invalid tensor dimensions or size overflow.");
      }
      total_size *= dim;
    }
  
    tensors.clear();
    tensors.reserve(nb_concurrency);
    const std::vector<T> data_values(total_size);
  
    for (int i = 0; i < nb_concurrency; ++i) {
      const Tensor data{in_name, tensors_dims, data_values};
      tensors.emplace_back(std::vector<Tensor>{data});
    }
  
    for (int i = 0; i < nb_concurrency; ++i) {
      starpu_memory_pin(tensors[i][0].data, tensors[i][0].size * tensors[i][0].data_size);
    }
  }

 private:
  static void TaskEndCallback(void *args);
  static void InitializeSessions(void *arg);
  static void CpuOrtInference(void *buffers[], void *args);
  static void CudaOrtInference(void *buffers[], void *args);
  static void InitializeSessions(std::vector<int> combined_worker_ids, void *arg);

  void CreateCombinedWorkers(int num_combined_workers);
  void BuildSubGraphFromOnnx(int index, const onnx::GraphProto& graph_proto);
  void UpdateTensorData(const std::vector<Tensor>& tensors, std::unordered_map<std::string, Tensor>& tensors_map);
  void Submit(int index, int priority, int worker_id, int num_inference, int &is_running, starpu_codelet &codelet, bool is_sync,
    std::vector<std::string> &in_names, std::vector<std::string> &out_names, GraphSubmit& graph_submit, std::vector<Tensor *> &inputs, 
    std::vector<Tensor *> &outputs);

  uint32_t OrtFootprint(struct starpu_task*);
  std::vector<Tensor*> CreateSubGraphTensors(const std::vector<Tensor*>& tensors, std::unordered_map<std::string, Tensor>& tensors_map);
  LayerInfo *InitializeLayerInformation(int index, size_t &size, size_t &num_buffers, std::vector<Tensor *> &inputs, std::vector<Tensor *> &outputs);
  CallbackInfo* InitializeCallbackInformation(std::string &in_name, std::string &out_name, int index, int &is_running, GraphSubmit& graph_submit, 
    std::vector<Tensor*>& inputs, std::vector<Tensor*>& outputs);

 public:
  std::vector<int> combined_workers_ids;

 private:
  int combined_idx_ = 0;
  int nb_combined_workers_;
  std::vector<int> firsts_workers_ids_;
  Ort::Env *ort_env_;
  std::string path_;
  std::string model_name_;
  std::unordered_map<int, int> idx_sessions_;
  std::unordered_map<int, int> idx_bindings_;
  std::unordered_map<std::string, Tensor> tensors_;
  std::unordered_map<int, struct starpu_codelet> codelets_;
  std::unordered_map<std::string, struct starpu_perfmodel> perf_models_;
  std::list<GraphSubmit> graph_submits_;
  std::vector<SubGraph> sub_graphs_;
  std::vector<double> latences_;
  std::vector<int> workers_ids_;
  std::vector<std::vector<Ort::Session>> sessions_;
  std::vector<std::vector<Ort::IoBinding>> bindings_;
};

#endif