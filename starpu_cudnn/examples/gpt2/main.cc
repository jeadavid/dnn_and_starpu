#include <getopt.h>

#include <cmath>
#include <queue>
#include <ctime>
#include <thread>
#include <random>
#include <iostream>

#include "../../src/graph.h"
#include "../../src/utils.h"

#define processor_id 0

const bool IS_SYNC = 0;

const OrtApi* g_ort = OrtGetApiBase()->GetApi(ORT_API_VERSION);

void onnxGptTest(std::string path, std::string in_name, std::string out_name, std::string mask_name);
int starPuOnnxGPT(std::string path, std::string in_name, std::string out_name, std::string mask_name);
void replaceTensorData(std::vector<Tensor> &pined, const std::vector<Tensor> &new_tensor);

int main()
{
  std::string out_name = "2351";
  std::string in_name = "input_ids";
  std::string mask_name = "attention_mask";
  std::string model_path = "../data/model/gpt2/gpt2.onnx";
  std::string model_path2 = "../data/model/gpt2/";

  //onnxGptTest(model_path, in_name, out_name, mask_name);
  starPuOnnxGPT(model_path2, in_name, out_name, mask_name);
}

void onnxGptTest(std::string path, std::string in_name, std::string out_name, std::string mask_name) {
  Ort::Env env(ORT_LOGGING_LEVEL_WARNING, "GPT2");
  Ort::SessionOptions session_options;
  session_options.SetGraphOptimizationLevel(GraphOptimizationLevel::ORT_ENABLE_EXTENDED);

  // Load the model
  Ort::Session session(env, path.c_str(), session_options);

  const int vocabulary_size = 50257;
  const int sequence_length = 128;
  const int batch_size = 1;

  // Prepare input tensors
  std::vector<int64_t> input_ids(sequence_length);
  std::vector<float> attention_mask(sequence_length, 1.0f);

  // Random number generation for input_ids
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(0, vocabulary_size - 1);
  for (int i = 0; i < sequence_length; ++i) {
      input_ids[i] = distrib(gen); // Generate a random token ID
  }

  auto memory_info = Ort::MemoryInfo::CreateCpu(OrtArenaAllocator, OrtMemTypeDefault);

  // Create tensors for input_ids and attention_mask
  std::vector<int64_t> input_ids_shape = {batch_size, sequence_length};
  std::vector<int64_t> attention_mask_shape = {batch_size, sequence_length};
  std::vector<Ort::Value> input_tensors;

  input_tensors.push_back(Ort::Value::CreateTensor<int64_t>(memory_info, input_ids.data(), input_ids.size(), input_ids_shape.data(), input_ids_shape.size()));
  input_tensors.push_back(Ort::Value::CreateTensor<float>(memory_info, attention_mask.data(), attention_mask.size(), attention_mask_shape.data(), attention_mask_shape.size()));

  // Run the model
  std::vector<const char*> input_names = {in_name.c_str(), mask_name.c_str()};
  std::vector<const char*> output_names = {out_name.c_str()};
  auto output_tensors = session.Run(Ort::RunOptions{nullptr}, input_names.data(), input_tensors.data(), input_tensors.size(), output_names.data(), output_names.size());

  Ort::Value& output_tensor = output_tensors.front();
  float* float_array = output_tensor.GetTensorMutableData<float>();
  auto output_shape = output_tensor.GetTensorTypeAndShapeInfo().GetShape();

  int vocab_size = 768;
  for (int i = 0; i < batch_size; ++i) 
  {
    std::cout << "Batch " << i << ":\n";
    for (int j = 0; j < sequence_length; ++j) 
    {
      int max_index = std::distance(float_array, std::max_element(float_array, float_array + vocab_size));
      float_array += vocab_size;
      std::cout << "Token " << j << ": " << max_index << "\n";
    }
  }
}

int starPuOnnxGPT(std::string path, std::string in_name, std::string out_name, std::string mask_name){
  Ort::Env env(ORT_LOGGING_LEVEL_WARNING, "Inference");

  int num_concurency = 32;
  int batch = 1;
  int num_subgraph = 1;

  int ret = starpu_init(NULL);
  if (ret == -ENODEV) {
    fprintf(stderr, "Aucun travailleur ne peut exécuter cette tâche.\n");
    return 0;
  }

  Graph graph(&env, path, num_subgraph);
  graph.InitializeRun(30, 0);

  starpu_set_limit_max_submitted_tasks(num_concurency);
  starpu_set_limit_min_submitted_tasks(num_concurency);

  //Pin the memory
  std::vector<std::vector<Tensor>> pined_tensors;
  std::vector<bool> Input_status(num_concurency);
  std::vector<int64_t> dimensions = {1, 128};
  graph.AllocateConcurrency<int64_t>(num_concurency, in_name, dimensions, pined_tensors);

  std::vector<std::vector<Tensor>> pined_mask;
  std::vector<bool> mask_status(num_concurency);
  std::vector<int64_t> MaskDimensions = {1, 128};
  graph.AllocateConcurrency<float>(num_concurency, mask_name, MaskDimensions, pined_mask);

  std::vector<int> is_running(Input_status.size(), false);
  std::vector<float> data_output(batch * 128 * 768);

  Tensor output{out_name, {batch, 128, 768}, data_output};
  std::vector<Tensor> outputs{output};

  std::vector<int64_t> ints(batch * 128, 150);
  Tensor input{in_name, {batch, 128}, ints};
  std::vector<Tensor> temp_input{input};
  replaceTensorData(pined_tensors[0], temp_input);

  std::vector<float> floats(batch * 128, 1.0f);
  Tensor mask{mask_name, {batch, 128}, floats};
  std::vector<Tensor> temp_mask{mask};
  replaceTensorData(pined_mask[0], temp_mask);

  std::vector<std::string> in_names = {in_name, mask_name};
  std::vector<std::string> out_names = {out_name};
  std::vector<Tensor> in_tensors = {pined_tensors[0][0], pined_mask[0][0]};
  graph.Run(batch, 0, 0, processor_id, 0, is_running[0], IS_SYNC, in_names, out_names, in_tensors, outputs);
  starpu_task_wait_for_all();
  std::cout << "Inference ended" << std::endl;

  graph.FreeGraph();
  std::cout << "StarPu Shutdown" << std::endl;
  starpu_shutdown();
  std::cout << "Cuda device synchronize" << std::endl;
  cudaDeviceSynchronize();
  std::cout << "Cuda device reset" << std::endl;
  cudaDeviceReset();

  return 0;
}

void replaceTensorData(std::vector<Tensor> &pined, const std::vector<Tensor> &new_tensor) {
  for(std::size_t i = 0; i < pined.size(); ++i)
  {
    memcpy(pined[i].data, new_tensor[i].data, new_tensor[i].TotalSizeInBytes());
  }
}