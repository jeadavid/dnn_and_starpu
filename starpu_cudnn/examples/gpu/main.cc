#include <getopt.h>

#include <cmath>
#include <queue>
#include <ctime>
#include <thread>
#include <random>
#include <iostream>
#include <fstream>

#include "../../src/onnx_benchmark.h"
#include "../../src/graph.h"
#include "../../src/utils.h"

#define processor_id 0

const int NB_TASKS = 200;
const int IMAGE_HEIGHT = 224;
const int IMAGE_WIDTH = 224;
const int IMAGE_CHANNELS = 3;
const int OUTPUT_SIZE = 1000;
const bool IS_SYNC = 0;

const OrtApi* g_ort = OrtGetApiBase()->GetApi(ORT_API_VERSION);

void replaceTensorData(std::vector<Tensor> &pined, const std::vector<Tensor> &new_tensor);
int starPuOnnx(std::string& path, std::string& in_name, std::string& out_name, int num_subgraph, int batch, int num_concurrency, std::ofstream &outputFile);
void warmup(int batch, std::string &in_name, std::string &out_name, Graph &graph, std::vector<std::vector<Tensor>> &pined_tensors);
void performInference(int num_task, int num_concurrency, int batch, std::string &in_name, std::string &out_name, Graph &graph, std::vector<std::vector<Tensor>> &pined_tensors, std::ofstream &outputFile);

int main(int argc, char *argv[]) {
  int concurrency = 0, batch = 0, subgraph = 0;
  std::string model, trace;

  static struct option long_options[] = {
    {"concurrency",    required_argument, 0, 'y'},
    {"batch",          required_argument, 0, 'b'},
    {"subgraph",       required_argument, 0, 's'},
    {"model",          required_argument, 0, 'm'},
    {0, 0, 0, 0}
  };

  int opt;
  int option_index = 0;
  while ((opt = getopt_long(argc, argv, "y:b:s:m:t:", long_options, &option_index)) != -1) {
    switch (opt) {
      case 'y':
        concurrency = std::stoi(optarg);
        break;
      case 'b':
        batch = std::stoi(optarg);
        break;
      case 's':
        subgraph = std::stoi(optarg);
        break;
      case 'm':
        model = optarg;
        break;
      case 't':
        trace = optarg;
        break;
      default:
        std::cerr << "Usage: " << argv[0] << " --concurrency <num> --batch <num> --subgraph <num> --model <path>\n";
        return 1;
    }
  }

  std::cout << "Concurrency: " << concurrency << "\n";
  std::cout << "Batch size: " << batch << "\n";
  std::cout << "Subgraph: " << subgraph << "\n";
  std::cout << "Model path: " << model << "\n";

  std::string in_name = "x";
  std::string out_name = "1158";
  std::string outputFilePath = "starpu_gpu.txt";

  std::ifstream infile(outputFilePath);
  bool isEmpty = infile.peek() == std::ifstream::traits_type::eof();
  infile.close(); // Important de fermer le flux d'entrée

  std::ofstream outputFile(outputFilePath, std::ios_base::app);
  if (!outputFile.is_open()) {
    std::cerr << "Erreur lors de l'ouverture du fichier de sortie." << std::endl;
    return 1;
  }

  if (isEmpty) {
    outputFile << "cpu;gpu;async;model;batch;concurrency;througput;p5;p50;p90;p95;p99\n";
    outputFile.flush();
  }

  starPuOnnx(model, in_name, out_name, subgraph, batch, concurrency, outputFile);

  outputFile.close();
}

bool CheckStatusAndReport(OrtStatus* status) {
  if (status) {
    std::cerr << g_ort->GetErrorMessage(status) << std::endl;
    return false;
  }

  return true;
}

int starPuOnnx(std::string& path, std::string& in_name, std::string& out_name, int num_subgraph, int batch, int num_concurrency, std::ofstream &outputFile)
{
  Ort::Env env(ORT_LOGGING_LEVEL_WARNING, "Inference");

  int ret = starpu_init(NULL);
  if (ret == -ENODEV) {
    fprintf(stderr, "Aucun travailleur ne peut exécuter cette tâche.\n");
    return 0;
  }

  Graph graph(&env, path, num_subgraph);
  graph.InitializeRun(NB_TASKS, 0);
  starpu_set_limit_max_submitted_tasks(num_concurrency);
  starpu_set_limit_min_submitted_tasks(num_concurrency);

  // Pin the memory
  std::vector<std::vector<Tensor>> pined_tensors;
  std::vector<bool> status(num_concurrency);
  std::vector<int64_t> dimensions = {batch, IMAGE_CHANNELS, IMAGE_WIDTH, IMAGE_HEIGHT};
  graph.AllocateConcurrency<float>(num_concurrency, in_name, dimensions, pined_tensors);
  
  warmup(batch, in_name, out_name, graph, pined_tensors);
  std::cout<<"Onnx warmup ended\n";
  sleep(1);

  performInference(NB_TASKS, num_concurrency, batch, in_name, out_name, graph, pined_tensors, outputFile);

  std::cout << "StarPu Shutdown" << std::endl;
  starpu_shutdown();

  std::cout << "Cuda device synchronize" << std::endl;
  cudaDeviceSynchronize();

  std::cout << "Cuda device reset" << std::endl;
  cudaDeviceReset();

  return 0;
}


void warmup(int batch, std::string &in_name, std::string &out_name, Graph &graph, std::vector<std::vector<Tensor>> &pined_tensors)
{
  std::vector<int> is_running(pined_tensors.size(), false);
  std::vector<float> data_output(batch * OUTPUT_SIZE);

  Tensor output{out_name, {batch, OUTPUT_SIZE}, data_output};
  std::vector<Tensor> outputs{output};

  std::vector<float> ints(batch * IMAGE_CHANNELS * IMAGE_HEIGHT * IMAGE_WIDTH, 150.0f);
  Tensor data{in_name, {batch, IMAGE_CHANNELS, IMAGE_HEIGHT, IMAGE_WIDTH}, ints};
  std::vector<Tensor> temp_vec{data};

  replaceTensorData(pined_tensors[0], temp_vec);

  std::vector<std::string> in_names = {in_name};
  std::vector<std::string> out_names = {out_name};
  graph.Run(batch, 0, 0, processor_id, 0, is_running[0], IS_SYNC, in_names, out_names, pined_tensors[0], outputs);
}

void performInference(int num_task, int num_concurrency, int batch, std::string &in_name, std::string &out_name, Graph &graph, std::vector<std::vector<Tensor>> &pined_tensors, std::ofstream &outputFile)
{
  std::vector<int> is_running(pined_tensors.size());

  // Generate the inputs tensors
  std::queue<std::vector<Tensor>> waiting_tasks;
  for(int i = 0; i < num_task; ++i) {
    std::vector<float> ints;
    ints.reserve(batch * IMAGE_CHANNELS * IMAGE_HEIGHT * IMAGE_WIDTH);

    std::mt19937 gen(static_cast<unsigned int>(std::time(nullptr)));
    std::uniform_real_distribution<float> dis(0.0, 255.0);
    
    for(int j = 0; j < batch * IMAGE_CHANNELS * IMAGE_HEIGHT * IMAGE_WIDTH; ++j) {
      ints.push_back(dis(gen));
    }

    Tensor data{in_name, {batch, IMAGE_CHANNELS, IMAGE_HEIGHT, IMAGE_WIDTH}, ints};
    std::vector<Tensor> temp_vec{data};
    waiting_tasks.push(std::move(temp_vec));
  }

  std::vector<std::vector<Tensor>> outputs(num_task);
  for(int i=0; i<num_task; ++i) {
    std::vector<float> data_output(batch * OUTPUT_SIZE);
    Tensor data{out_name, {batch, OUTPUT_SIZE}, data_output};
    std::vector<Tensor> temp_vec{data};
    outputs[i] = temp_vec;
  }

  // Launch the inferences
  int idx;
  std::vector<std::string> in_names = {in_name};
  std::vector<std::string> out_names = {out_name};
  auto start_gpu = std::chrono::high_resolution_clock::now();
  for(idx=0; idx<num_concurrency; ++idx) {
    is_running[idx] = true;
    waiting_tasks.pop();
    graph.Run(batch, idx, num_task-idx-1, processor_id, idx, is_running[idx], IS_SYNC, in_names, out_names, pined_tensors[idx], outputs[idx]);
  }

  while(!waiting_tasks.empty()){
    for (std::size_t i = 0; i < is_running.size(); ++i) {
      if(is_running[i] && !waiting_tasks.empty()) {
        is_running[i] = true;
        waiting_tasks.pop();
        graph.Run(batch, idx, num_task-idx-1, processor_id, idx, is_running[i], IS_SYNC, in_names, out_names, pined_tensors[i], outputs[idx]);
        idx++;
      }
    }
  }

std::cout << "Start to wait" << std::endl;
starpu_task_wait_for_all();
auto end = std::chrono::high_resolution_clock::now();
auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start_gpu);
std::cout<<"Inference duration : "<<duration.count()/num_task<< "ns Debit : " << 1.0/(duration.count()/(num_task*batch)/1e9) << std::endl;

std::vector<double> latences = graph.GetLatences();

std::cout << "Latence p_50 : " << Utils::Percentile(latences,0.50)/1.0e3 << " ms"<< std::endl;
std::cout << "Latence p_90 : " << Utils::Percentile(latences,0.90)/1.0e3 << " ms"<< std::endl;
std::cout << "Latence p_95 : " << Utils::Percentile(latences,0.95)/1.0e3 << " ms"<< std::endl;
std::cout << "Latence p_99 : " << Utils::Percentile(latences,0.99)/1.0e3 << " ms"<< std::endl;
outputFile << 1 << ";" << 0 << ";" << 1 << ";" << "googlenet_cpu" << ";" << batch << ";" << num_concurrency << ";" << 1.0/(duration.count()/(num_task*batch)/1e9) << ";" << Utils::Percentile(latences,0.05) << ";" << Utils::Percentile(latences,0.50) << ";" << Utils::Percentile(latences,0.90) << ";" << Utils::Percentile(latences,0.95) << ";" << Utils::Percentile(latences,0.99) << "\n";
outputFile.flush();
graph.FreeGraph();
}

void replaceTensorData(std::vector<Tensor> &pined, const std::vector<Tensor> &new_tensor) {
  for(std::size_t i = 0; i < pined.size(); ++i)
  {
    memcpy(pined[i].data, new_tensor[i].data, new_tensor[i].TotalSizeInBytes());
  }
}