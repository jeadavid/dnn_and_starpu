#include <getopt.h>

#include <cmath>
#include <queue>
#include <ctime>
#include <thread>
#include <random>
#include <iostream>

#include "../../src/graph.h"
#include "../../src/utils.h"

#define processor_id 1

const int NB_TASKS = 30;
const int IMAGE_HEIGHT = 224;
const int IMAGE_WIDTH = 224;
const int IMAGE_CHANNELS = 3;
const int OUTPUT_SIZE = 1000;
const bool IS_SYNC = 0;

const OrtApi* g_ort = OrtGetApiBase()->GetApi(ORT_API_VERSION);

int starPuOnnx(std::string& path, std::string& in_name, std::string& out_name, int num_graph, int batch, int num_concurrency, int combined_worker);
void performInference(int num_task, int num_concurrency, int batch, std::string &in_name, std::string &out_name, Graph &graph, 
  std::vector<std::vector<Tensor>> &pined_tensors);

int main(int argc, char *argv[]) {
  int combined_worker = 0, concurrency = 0, batch = 0, subgraph = 0;
  std::string model, trace;

  static struct option long_options[] = {
    {"combinedWorker", required_argument, 0, 'c'},
    {"concurrency",    required_argument, 0, 'y'},
    {"batch",          required_argument, 0, 'b'},
    {"subgraph",       required_argument, 0, 's'},
    {"model",          required_argument, 0, 'm'},
    {0, 0, 0, 0}
  };

  int opt;
  int option_index = 0;
  while ((opt = getopt_long(argc, argv, "c:y:b:s:m:t:", long_options, &option_index)) != -1) {
    switch (opt) {
      case 'c':
        combined_worker = std::stoi(optarg);
        break;
      case 'y':
        concurrency = std::stoi(optarg);
        break;
      case 'b':
        batch = std::stoi(optarg);
        break;
      case 's':
        subgraph = std::stoi(optarg);
        break;
      case 'm':
        model = optarg;
        break;
      case 't':
        trace = optarg;
        break;
      default:
        std::cerr << "Usage: " << argv[0] << " --combinedWorker <num> --concurrency <num> --batch <num> --subgraph <num> --model <path>\n";
        return 1;
    }
  }

  std::cout << "Combined Worker: " << combined_worker << "\n";
  std::cout << "Concurrency: " << concurrency << "\n";
  std::cout << "Batch size: " << batch << "\n";
  std::cout << "Subgraph: " << subgraph << "\n";
  std::cout << "Model path: " << model << "\n";

  std::string in_name = "input";
  std::string out_name = "output";
  starPuOnnx(model, in_name, out_name, subgraph, batch, concurrency, combined_worker);
}

bool CheckStatusAndReport(OrtStatus* status) {
  if (status) {
    std::cerr << g_ort->GetErrorMessage(status) << std::endl;
    return false;
  }
  return true;
}

int starPuOnnx(std::string& path, std::string& in_name, std::string& out_name, int num_graph, int batch, int num_concurrency, int combined_worker)
{
  OrtThreadingOptions* tp_options;
  OrtStatus* statuss;

  statuss = g_ort->CreateThreadingOptions(&tp_options);
  if (!CheckStatusAndReport(statuss)) return -1;
  statuss = g_ort->SetGlobalSpinControl(tp_options, 0);
  if (!CheckStatusAndReport(statuss)) return -1;
  //for plafrim p100
  //statuss = g_ort->SetGlobalIntraOpThreadAffinity(tp_options, "10;11;12;13;14;15;16;17;18;19;20;21;22;23;24;25;26;27;28;29;30;31;32");
  statuss = g_ort->SetGlobalIntraOpThreadAffinity(tp_options, "1;2;3;4;5;6;7;8;9;10;11");
  if (!CheckStatusAndReport(statuss)) return -1;
  statuss = g_ort->SetGlobalIntraOpNumThreads(tp_options, 12);
  if (!CheckStatusAndReport(statuss)) return -1;
  statuss = g_ort->SetGlobalInterOpNumThreads(tp_options, 1);
  if (!CheckStatusAndReport(statuss)) return -1;

  Ort::Env env(tp_options, ORT_LOGGING_LEVEL_WARNING, "Inference");
  g_ort->ReleaseThreadingOptions(tp_options);

  Ort::SessionOptions sessionOptions;
  sessionOptions.DisablePerSessionThreads();

  int ret = starpu_init(NULL);
  if (ret == -ENODEV) {
    fprintf(stderr, "Aucun travailleur ne peut exécuter cette tâche.\n");
    return 0;
  }

  Graph graph(&env, path, num_graph);
  graph.InitializeRun(NB_TASKS, combined_worker);
  starpu_set_limit_max_submitted_tasks(num_concurrency);
  starpu_set_limit_min_submitted_tasks(num_concurrency);

  //Pin the memory
  std::vector<std::vector<Tensor>> pined_tensors;
  std::vector<bool> status(num_concurrency);
  std::vector<int64_t> dimensions = {batch, IMAGE_CHANNELS, IMAGE_WIDTH, IMAGE_HEIGHT};
  graph.AllocateConcurrency<float>(num_concurrency, in_name, dimensions, pined_tensors);
  
  performInference(NB_TASKS, num_concurrency, batch, in_name, out_name, graph, pined_tensors);

  std::cout << "StarPu Shutdown" << std::endl;
  starpu_shutdown();

  return 0;
}

void performInference(int num_task, int num_concurrency, int batch, std::string &in_name, std::string &out_name, Graph &graph, std::vector<std::vector<Tensor>> &pined_tensors)
{  
  std::cout << "Perform Inference" << std::endl;
  std::vector<int> is_running(pined_tensors.size(), false);

  //Préparer les tâches
  std::queue<std::vector<Tensor>> waiting_tasks;
  for(int i = 0; i < num_task; ++i) {
    std::vector<float> ints;
    ints.reserve(batch * IMAGE_CHANNELS * IMAGE_HEIGHT * IMAGE_WIDTH);

    std::mt19937 gen(static_cast<unsigned int>(std::time(nullptr)));
    std::uniform_real_distribution<float> dis(0.0, 255.0);
    
    for(int j = 0; j < batch * IMAGE_CHANNELS * IMAGE_HEIGHT * IMAGE_WIDTH; ++j) {
      ints.push_back(dis(gen));
    }

    Tensor data{in_name, {batch, IMAGE_CHANNELS, IMAGE_HEIGHT, IMAGE_WIDTH}, ints};
    std::vector<Tensor> temp_vec{data};
    waiting_tasks.push(std::move(temp_vec));
  }

  std::vector<std::vector<Tensor>> outputs(num_task);
  for(int i=0; i<num_task; ++i) {
    std::vector<float> data_out(batch * OUTPUT_SIZE);
    Tensor data{out_name, {batch, OUTPUT_SIZE}, data_out};
    std::vector<Tensor> temp_vec{data};
    outputs[i] = temp_vec;
  }

  // Launch the inferences
  int idx;
  std::vector<std::string> in_names = {in_name};
  std::vector<std::string> out_names = {out_name};
  auto startGPU = std::chrono::high_resolution_clock::now();
  for(idx=0; idx<num_concurrency; ++idx) {
    is_running[idx] = true;
    waiting_tasks.pop();
    graph.Run(batch, idx, num_task-idx-1, processor_id, idx, is_running[idx], IS_SYNC, in_names, out_names, pined_tensors[idx], outputs[idx]);
  }

  while(!waiting_tasks.empty()){
    for (std::size_t i = 0; i < is_running.size(); ++i) {
      if(is_running[i] && !waiting_tasks.empty()) {
        is_running[i] = true;
        waiting_tasks.pop();
        graph.Run(batch, idx, num_task-idx-1, processor_id, idx, is_running[i], IS_SYNC, in_names, out_names, pined_tensors[i], outputs[idx]);
        idx++;
      }
    }
  }

  std::cout << "Start to wait" << std::endl;
  starpu_task_wait_for_all();
  auto end = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - startGPU);
  std::cout<<"Inference duration : "<<duration.count()/num_task<< "ns Debit : " << 1.0/(duration.count()/(num_task*batch)/1e9) << std::endl;

  std::vector<double> latences = graph.GetLatences();

  std::cout << "Latence p_50 : " << Utils::Percentile(latences,0.50)/1.0e3 << " ms"  << std::endl;
  std::cout << "Latence p_90 : " << Utils::Percentile(latences,0.90)/1.0e3 << " ms"  << std::endl;
  std::cout << "Latence p_95 : " << Utils::Percentile(latences,0.95)/1.0e3 << " ms"  << std::endl;
  std::cout << "Latence p_99 : " << Utils::Percentile(latences,0.99)/1.0e3 << " ms"  << std::endl;

  graph.FreeGraph();
}